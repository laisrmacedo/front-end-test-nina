import { Component, OnInit } from '@angular/core';
import { dataObject, filterObject } from 'src/app/interfaces/data.interface';
import { FilterService } from 'src/app/services/filter.service';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'dashboard';
  dataToRender: dataObject[] = []

  
  constructor(
    private filterService: FilterService,
    private dataService: DataService
    ) { }
    
    filterPlaces: filterObject = this.filterService.filterPlaces
    filterTypes: filterObject = this.filterService.filterTypes
    filterMoment: filterObject = this.filterService.filterMoment
    filterGenders: filterObject = this.filterService.filterGenders
    
    data$ = this.dataService.data$

  ngOnInit(): void {
    // this.dataService.data$.subscribe((data) => {
    //   this.dataToRender = data;
    //   console.log("app", this.dataToRender)
    // });
  }

  onClickToFilter() {
    return this.filterService.toFilter()
  }

  onClickToCleanFilter() {
    return this.filterService.toCleanFilter()
  }
}
