import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts/highstock';
import { dataObject } from 'src/app/interfaces/data.interface';
import { HighchartsSettings } from 'src/app/services/highcharts-settings.service';

@Component({
  selector: 'app-bar-chart-gender',
  templateUrl: './bar-chart-gender.component.html',
  styleUrls: ['./bar-chart-gender.component.css']
})

export class BarChartGenderComponent implements OnInit {
  constructor(private highchartsSettings: HighchartsSettings) { }
  @Input() genderChartData: dataObject[] | null = [];



  chartConstructor: string = 'chart';
  updateFlag: boolean = false;
  oneToOneFlag: boolean = true;
  runOutsideAngular: boolean = false;

  Highcharts: typeof Highcharts = Highcharts;


  // chartOptions: Highcharts.Options = {
  chartOptions: any = {
    xAxis: {
      categories: ['Mulheres', 'Homens', 'Não-binários'],
    },
    yAxis: {
      // categories: ['0', '10', '20', '30', '40'],
      gridLineColor: '#747474',
      tickPositions: [0, 10, 20, 30, 40]
    },
    title: {
      text: ''
    },
    plotOptions: {
      column: {
        borderWidth: 0
      }
    },
    series: [{
      name: 'Cisgênero',
      data: [],
      type: 'column',
      color: '#5B43D9'
    },
    {
      name: 'Transgênero',
      data: [],
      type: 'column',
      color: '#B3A8EB'
    }
    ]
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes['genderChartData'] && this.genderChartData) {
      const allGenders: string[] = this.genderChartData.map((item) => item["victim_gender"])
      const womanTrans: string[] = allGenders.filter((gender) => gender === 'woman_trans')
      const womanCis: string[] = allGenders.filter((gender) => gender === 'woman_cis')
      const manTrans: string[] = allGenders.filter((gender) => gender === 'man_trans')
      const manCis: string[] = allGenders.filter((gender) => gender === 'man_cis')
      const nonBinary: string[] = allGenders.filter((gender) => gender === 'non-binary')

      if (this.chartOptions.series) {
        this.chartOptions.series[0].data = [womanCis.length, manCis.length, nonBinary.length];
        this.chartOptions.series[1].data = [womanTrans.length, manTrans.length, nonBinary.length];
        // this.chartOptions.series[1].data = ;
        // console.log(this.chartOptions.series[0].data)
      }

      this.updateFlag = true;
    }
  }

  ngOnInit(): void {
    Highcharts.setOptions(this.highchartsSettings.highchartsOptions)
  }
}
