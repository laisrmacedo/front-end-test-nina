import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { dataObject } from 'src/app/interfaces/data.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-occurrence-card',
  templateUrl: './occurrence-card.component.html',
  styleUrls: ['./occurrence-card.component.css']
})
export class OccurrenceCardComponent implements OnChanges {
  @Input() originalData: dataObject[] | null = [];

  refactorData: dataObject[] = [];

  arrayValues = [
    'bus',
    'bus_terminal',
    'subway',
    'bus_stop',
    'subway_terminal',
    'groping',
    'stalking',
    'unwanted_photos',
    'verbal_aggression',
    'physical_aggression'
  ];

  arrayNewValues = [
    'Ônibus',
    'Terminal de Ônibus',
    'Metrô',
    'Parada de Ônibus',
    'Terminal de Metrô',
    'Importunação sexual',
    'Perseguição',
    'Fotos indesejadas',
    'Agressão verbal',
    'Agressão física'
  ];

  refactor = () => {
    this.refactorData = this.originalData?.map(item => {
      const newItem = JSON.parse(JSON.stringify(item))
      const place = this.arrayValues.indexOf(newItem.place)
      const type = this.arrayValues.indexOf(newItem.type[0])
  
      if (place !== -1) {
        newItem.place = this.arrayNewValues[place]
      }
      if (type !== -1) {
        newItem.type[0] = this.arrayNewValues[type]
      }
  
      const regexPattern = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;
      if (regexPattern.test(newItem.created_at)) {
        newItem.created_at = this.getDate(newItem.created_at)
      }
  
      return newItem;
    }) || [];
  }

  getDate = (date: string) => {
    const timeOccurrence = Date.parse(date)
    const day = new Date(timeOccurrence).getDate() < 10? '0' + new Date(timeOccurrence).getDate() : new Date(timeOccurrence).getDate()
    const month = new Date(timeOccurrence).getMonth() < 9? '0' + Number(new Date(timeOccurrence).getMonth() + 1) : Number(new Date(timeOccurrence).getMonth() + 1)
    return day + '/' + month + '/' + new Date(timeOccurrence).getFullYear()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['originalData'] && this.originalData) {
      this.refactorData = [...this.originalData];
      this.refactor()
    }
  }

  // ngOnInit(): void {
  //   // this.refactor()
  // }  
}