import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OccurrenceCardComponent } from './occurrence-card.component';

describe('OccurrenceCardComponent', () => {
  let component: OccurrenceCardComponent;
  let fixture: ComponentFixture<OccurrenceCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OccurrenceCardComponent]
    });
    fixture = TestBed.createComponent(OccurrenceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
