import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HighchartsSettings } from 'src/app/services/highcharts-settings.service';
import * as Highcharts from 'highcharts';
import { dataObject } from 'src/app/interfaces/data.interface';
declare var require: any;
require('highcharts/modules/heatmap')(Highcharts);


@Component({
    selector: 'app-heatmap-chart',
    templateUrl: './heatmap-chart.component.html',
    styleUrls: ['./heatmap-chart.component.css']
})
export class HeatmapChartComponent implements OnInit, OnChanges {
    constructor(private highchartsSettings: HighchartsSettings) {
        const x = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
        const y = [0, 1, 2, 3, 4, 5, 6];

        x.forEach(x => {
            y.forEach(y => {
                this.possibleCoordinates.push([x, y]);
            });
        });
     }
    @Input() heatmapChartData: dataObject[] | null = [];


    chartConstructor: string = 'chart';
    updateFlag: boolean = false;
    oneToOneFlag: boolean = true;
    runOutsideAngular: boolean = false;

    Highcharts: typeof Highcharts = Highcharts;

    // chartOptions: Highcharts.Options = {
    chartOptions: any = {

        chart: {
            type: 'heatmap',
            marginRight: 40,
            plotBorderWidth: 5,
            plotBorderColor: '#303030',
        },
        title: {
            text: ''
        },

        xAxis: {
            // categories: ['8h', '9h', '10h', '11h', '12h', '13h', '14h', '15h', '16h', '17h'],
            reversed: false
        },

        yAxis: {
            categories: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'],
            reversed: false
        },

        colorAxis: {
            reversed: false,
            min: 0,
            max: 4,
            minColor: '#f9f9f9',
            maxColor: '#3C24B5',
            labels: {
                style: {
                    color: '#f9f9f9',
                    fontSize: '10px'
                }
            }
        },
        tooltip: {
            format: '<b>{point.value}</b> ocorrências<br>' +
                '<b>{series.yAxis.categories.(point.y)} às {point.x}h</b>'
        },
        legend: {
            align: 'right',
            layout: 'vertical',
            itemMarginTop: 40,
            verticalAlign: 'top',
            x: 27,
            y: 5,
            symbolHeight: 160,
        },
        plotOptions: {
            heatmap: {
                dataLabels: {
                    enabled: false,
                },
                borderWidth: 2,
                borderColor: '#303030'
            },
        },
        series: [{
            type: 'heatmap',
            name: 'Time',
            borderWidth: 0,
            data: [],
            dataLabels: {
                enabled: false,
            }
        }],
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['heatmapChartData'] && this.heatmapChartData) {
            const occurrenceTimeInterval = this.heatmapChartData.map(item => {
                return [new Date(item.datetime).getHours(), new Date(item.datetime).getDay()]
            }).filter(([x, _]) => x >= 8 && x <= 17)

            const coordinateCount: any = {};

            occurrenceTimeInterval.forEach(coordinate => {
                const key = coordinate.join('_')
                coordinateCount[key] = (coordinateCount[key] || 0) + 1
            })

            const arraysWithOccurrenceQuantity = Object.entries(coordinateCount).map(([key, count]) => {
                const coordinate = key.split('_').map(Number)
                return [...coordinate, count]
            })

            const allCoordinatesAndQuantity = this.possibleCoordinates.map(([x, y]) => {
                let newArray: number[] = [];
                let found = false;

                for (let array of arraysWithOccurrenceQuantity) {
                    if (array[0] === x && array[1] === y) {
                        newArray = [x, y, array[2] as number];
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    newArray = [x, y, 0];
                }

                return newArray;
            });

            if (this.chartOptions.series) {
                this.chartOptions.series[0].data = allCoordinatesAndQuantity
            }
            this.updateFlag = true
        }
    }

    possibleCoordinates: number[][] = [];

    ngOnInit(): void {
        Highcharts.setOptions(this.highchartsSettings.highchartsOptions)        
    }
}
