import { Component, Input } from '@angular/core';
import { filterObject } from 'src/app/interfaces/data.interface';
import { FilterService } from 'src/app/services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})

export class FilterComponent {
  @Input() data: filterObject = { key: '', label: '', defaultOption: '', arrayValues: [], arrayOptions: [] }
  
  constructor(
    private filterService: FilterService
    ) { }
    
  selectdValue: string = this.filterService.selectdValue
  
  onChangeSelect = () => {
    // console.log("filter", this.data)
    this.filterService.selectedFilters = {
      ...this.filterService.selectedFilters,
      [this.data.key]: this.selectdValue
    }
  }  
}