import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts/highstock';
import { dataObject } from 'src/app/interfaces/data.interface';
import { HighchartsSettings } from 'src/app/services/highcharts-settings.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})

export class BarChartComponent implements OnInit {
  constructor(private highchartsSettings: HighchartsSettings) {}
    @Input() ageChartData: dataObject[] | null = [];

    intialAge: dataObject[] = []  
    finalAge: dataObject[] = []

    chartConstructor: string = 'chart'; 
    updateFlag: boolean = false; 
    oneToOneFlag: boolean = true;
    runOutsideAngular: boolean = false;

    Highcharts: typeof Highcharts = Highcharts;

    
    // chartOptions: Highcharts.Options = {
    chartOptions: any = {
      xAxis: {
        categories: ['< 14', '14 - 18', '19 - 29', '30 - 39', '40 - 49', '50 - 60', '> 60'],
      },
      title: {
        text: ''
      },
      yAxis: {
        // categories: ['0', '20', '40', '60', '80', '100', '120', '140', '160', '180'],
        gridLineColor: '#747474'
      },
      plotOptions: {
        column: {
          borderWidth: 0 
        }
      },
      tooltip: {
        format: '<b>{series.xAxis.categories.(point.x)}</b><br/>' + '<b>{point.y} ocorrências</b><br/>'
      },
      series: [{
        name: 'Idade Confirmada',
        data: [],
        type: 'column',
        color: '#5B43D9'
      },
      {
        name: 'Idade estimada',
        data: [],
        type: 'column',
        color: '#B3A8EB'
      }
    ]
    };
    
    ngOnChanges(changes: SimpleChanges) {
      if (changes['ageChartData'] && this.ageChartData) {
        const allAges: number[] = this.ageChartData.map((item) => item["victim_age"])
        const bigger60: number[] = allAges.filter((item) => item > 60)
        const between60and50: number[] = allAges.filter((item) => item >= 50 && item <= 60)
        const between49and40: number[] = allAges.filter((item) => item >= 40 && item <= 49)
        const between39and30: number[] = allAges.filter((item) => item >= 30 && item <= 39)
        const between29and19: number[] = allAges.filter((item) => item >= 19 && item <= 29)
        const between18and14: number[] = allAges.filter((item) => item >= 14 && item <= 18)
        const smaller14: number[] = allAges.filter((item) => item < 14)
  
        if(this.chartOptions.series){
          this.chartOptions.series[0].data = [smaller14.length, between18and14.length, between29and19.length, between39and30.length, between49and40.length, between60and50.length, bigger60.length];
        }
  
        this.updateFlag = true;
      }
    }
    
  ngOnInit(): void {
    Highcharts.setOptions(this.highchartsSettings.highchartsOptions)
  }
}
