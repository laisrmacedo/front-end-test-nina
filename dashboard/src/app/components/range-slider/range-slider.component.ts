import { Component, Input, OnInit } from '@angular/core';
import { FilterService } from 'src/app/services/filter.service';

@Component({
  selector: 'app-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.css'],
})
export class RangeSliderComponent{

  @Input() minValue: number = 0;
  @Input() maxValue: number = 100;
  @Input() startValues: number[] = [20, 80];

  constructor(
    private filterService: FilterService
    ) { }

  onSliderChange = (): void => {
    this.filterService.selectedFilters = {
      ...this.filterService.selectedFilters,
      victim_age: {
        min: this.startValues[0],
        max: this.startValues[1]
      }
    }
  }
}
