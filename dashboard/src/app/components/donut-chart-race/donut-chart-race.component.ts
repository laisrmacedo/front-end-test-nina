import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts/highstock';
import { dataObject } from 'src/app/interfaces/data.interface';
import { HighchartsSettings } from 'src/app/services/highcharts-settings.service';

@Component({
  selector: 'app-donut-chart-race',
  templateUrl: './donut-chart-race.component.html',
  styleUrls: ['./donut-chart-race.component.css']
})
export class DonutChartRaceComponent implements OnChanges, OnInit {
  @Input() momentChartData: dataObject[] | null = [];

  atMomentTrue: dataObject[] = []  
  atMomentFalse: dataObject[] = []

  chartConstructor: string = 'chart';
  updateFlag: boolean = false;
  oneToOneFlag: boolean = true;
  runOutsideAngular: boolean = false;

  Highcharts: typeof Highcharts = Highcharts;

  // chartOptions: Highcharts.Options = {
  chartOptions: any = {
    chart: {
      type: 'pie',
      animation: {
        duration: 1500, 
      },
    },
    title: {
      text: ''
    },
    plotOptions: {
      pie: {
        innerSize: '75%',
        dataLabels: {
          enabled: true,
          crop: false,
          distance: '-70%',
          style: {
              fontWeight: 'bold',
              fontSize: '16px',
                  color: '#fff',
          },
          connectorWidth: 0
      },
        colors: ['#5B43D9', '#F9F9F9'],
        borderWidth: 0,
      },
    },
    tooltip: {
        format: `<b>{key}</b>: {y} ocorrências`
    },
    series: [
      {
        type: 'pie',
        name: '',
        center: ['20%', '50%'],
        data: this.createChartData(this.atMomentTrue.length, this.momentChartData? this.momentChartData.length : 0),
      },
      {
        type: 'pie',
        name: '',
        center: ['70%', '50%'],
        data: this.createChartData(this.atMomentFalse.length, this.momentChartData? this.momentChartData.length : 0), // Você pode ajustar isso conforme necessário
      },
    ],
  };

  private createChartData(trueCount: number, totalCount: number): Highcharts.PointOptionsObject[] {
    const percentage = (trueCount / totalCount) * 100;
    return [
      { name: `${this.momentChartData && this.momentChartData.length === 0 ? "" : percentage.toFixed(0) + '%'}`, y: trueCount },
      { name: ``, y: totalCount - trueCount },
    ];
  }

  constructor(
    private highchartsSettings: HighchartsSettings
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['momentChartData'] && this.momentChartData) {
      this.atMomentTrue = this.momentChartData?.filter(item => item["at_moment"]);
      this.atMomentFalse = this.momentChartData?.filter(item => !item["at_moment"]);

      if(this.chartOptions.series){
        this.chartOptions.series[0].data = this.createChartData(this.atMomentTrue.length, this.momentChartData?.length);
        this.chartOptions.series[1].data = this.createChartData(this.atMomentFalse.length, this.momentChartData?.length);
      }

      this.updateFlag = true;
    }
  }
  
  ngOnInit(): void {
    Highcharts.setOptions(this.highchartsSettings.highchartsOptions)
  }
}
