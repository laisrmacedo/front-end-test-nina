import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonutChartRaceComponent } from './donut-chart-race.component';

describe('DonutChartRaceComponent', () => {
  let component: DonutChartRaceComponent;
  let fixture: ComponentFixture<DonutChartRaceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DonutChartRaceComponent]
    });
    fixture = TestBed.createComponent(DonutChartRaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
