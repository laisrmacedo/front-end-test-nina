import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterComponent } from './components/filter/filter.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BarChartComponent } from './components/bar-chart-age/bar-chart.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { BarChartGenderComponent } from './components/bar-chart-gender/bar-chart-gender.component';
import { DonutChartRaceComponent } from './components/donut-chart-race/donut-chart-race.component';
import { HeatmapChartComponent } from './components/heatmap-chart/heatmap-chart.component';
import { DataService } from './services/data.service';
import { RangeSliderComponent } from './components/range-slider/range-slider.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import { OccurrenceCardComponent } from './components/occurrence-card/occurrence-card.component';

@NgModule({
  declarations: [
    AppComponent,
    FilterComponent,
    BarChartComponent,
    BarChartGenderComponent,
    DonutChartRaceComponent,
    HeatmapChartComponent,
    RangeSliderComponent,
    OccurrenceCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HighchartsChartModule,
    BrowserAnimationsModule,
    MatSliderModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
