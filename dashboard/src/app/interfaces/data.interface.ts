export interface filterObject {
  key: string,
  label: string,
  defaultOption: string,
  arrayValues: string[],
  arrayOptions: string[]
}

export interface dataObject {
  _id: string,
  place: string,
  at_moment: boolean,
  datetime: string,
  modified_at: string,
  created_at: string,
  description: string,
  situation: string[],
  type: string[],
  victim_gender: string,
  victim_age: number
}

export interface selectedFilter {
  place?: string,
  situation?: string,
  type?: string,
  victim_gender?: string,
  victim_age?: {
    min: number,
    max: number
  }
}