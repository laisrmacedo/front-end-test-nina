import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { dataObject, filterObject, selectedFilter } from '../interfaces/data.interface';

@Injectable({
  providedIn: 'root',
})

export class FilterService {
  selectedFilters : selectedFilter = {
    place: "",
    situation: "",
    type: "",
    victim_gender: "",
    victim_age: {
      min: 20,
      max: 80
    }
  }

  filterPlaces: filterObject = {
    key: "place",
    label: "Local das ocorrências",
    defaultOption: "Todos os locais",
    arrayValues: ['', 'bus', 'bus_terminal', 'subway', 'bus_stop', 'subway_terminal'],
    arrayOptions: ["Todos os locais", 'Ônibus', 'Terminal de Ônibus', 'Metrô', 'Parada de Ônibus', 'Terminal de Metrô']
  }

  filterTypes: filterObject = {
    key: "type",
    label: "Tipo de importunação",
    defaultOption: "Todos",
    arrayValues: ['', 'groping', 'stalking', 'unwanted_photos', 'verbal_aggression', 'physical_aggression'],
    arrayOptions: ["Todos", 'Importunação sexual', 'Perseguição', 'Fotos indesejadas', 'Agressão verbal', 'Agressão física']
  }

  filterMoment: filterObject = {
    key: "at_moment",
    label: "Momento da importunação",
    defaultOption: "Todos",
    arrayValues: ['', 'true', 'false'],
    arrayOptions: ["Todos", 'No momento', 'Após o ocorrido']
  }

  filterGenders: filterObject = {
    key: "victim_gender",
    label: "Gênero",
    defaultOption: "Todos",
    arrayValues: ['', 'woman_cis', 'man_cis', 'woman_trans', 'man_trans', 'non-binary'],
    arrayOptions: ["Todos", 'Mulher Cis', 'Homem Cis', 'Mulher Trans', 'Homem Trans', 'Não binário']
  }

  filteredData: dataObject[] | null = []
  selectdValue: string = ''

  constructor(
    private dataService: DataService
  ) { }

  toFilter = () => {
    this.dataService.getJsonData(this.selectedFilters)
  }
  
  toCleanFilter = () => {
    this.selectedFilters = {
      place: "",
      situation: "",
      type: "",
      victim_gender: "",
      victim_age: {
        min: 0,
        max: 100
      }
    }
    
    this.selectdValue = ''
    this.dataService.getJsonData(this.selectedFilters)
  }
}
