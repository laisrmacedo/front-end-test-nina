import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HighchartsSettings {
  public highchartsOptions: any = {
      colors: ['#DEDAF3', '#B3A8EB', '#8675E2', '#5F48DA', '#4F37CB', '#3C24B5'],
      chart: {
          backgroundColor: 'transparent',
          width: 420,
          height: 320,
          margin: [60, 0, 100, 40],
          reflow: true,
          borderWidth: 0,
      },
      legend: {
          itemStyle: {
              font: '12px Roboto, sans-serif',
              color: '#fff'
          },
          itemHoverStyle:{
              color: '#747474'
          }
      },
      xAxis: {
        labels: {
          style: {
            color: '#fff',
            fontSize: '12px'
          }
        }
      },
      yAxis: {
        labels: {
          style: {
            color: '#fff',
            fontSize: '12px'
          }
        }
      }
  };
}
