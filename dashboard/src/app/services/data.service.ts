import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { dataObject, selectedFilter } from '../interfaces/data.interface';
import jsonDB from '../../../../complaints-server/db.json';

@Injectable({
  providedIn: 'root',
})

export class DataService {
  private jsonData: dataObject[] = jsonDB.complaints

  private dataSubject = new BehaviorSubject<dataObject[]>([]);
  data$: Observable<dataObject[]> = this.dataSubject.asObservable();

  constructor() {
    this.dataSubject.next(this.jsonData)
  }

  updateData(newData: dataObject[]): void {
    this.dataSubject.next(newData);
  }

  getJsonData(params: selectedFilter): void {
    const paramsEntries: any[] = Object.entries(params).filter(([_, value]) => Boolean(value));
    let filteredJson: dataObject[] = this.jsonData.filter(item => {

      let allCriteriaMet = true
      paramsEntries.forEach(([key, value]) => {
        if (key === 'victim_age') {
          if (params['victim_age'] && (item['victim_age'] < params['victim_age'].min || item['victim_age'] > params['victim_age']?.max)) {
            allCriteriaMet = false;
          }
        } else if (key === 'type') {
          if (!item['type'].includes(value)) {
            allCriteriaMet = false;
          }
        } else if(key === 'at_moment'){
          if (item['at_moment'] !== JSON.parse(value)) {
            allCriteriaMet = false;
          }
        }else if ((key === 'victim_gender' || key === 'place') && item[key as keyof dataObject] !== value){
          allCriteriaMet = false;
        }
      })
      return allCriteriaMet;
    });
    this.updateData(filteredJson)
  }
}